package com.example.name_profile;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        insertItemsAtRuntime();
        insertSpecialItemsAtRuntime();
    }

    private void insertSpecialItemsAtRuntime() {
        // step 1: get the list view
        ListView listView = (ListView)findViewById(R.id.listViewMain);
        // step 2: create an array list
        ArrayList<MyData> dataArrayList = new ArrayList<MyData>();
        dataArrayList.add(new MyData("Do Nhat Huy", "www.hcmus.edu.vn"));
        dataArrayList.add(new MyData("Nguyen Quang Thuc", "www.fit.hcmus.edu.vn"));
        dataArrayList.add(new MyData("Nguyen Quang Thuc", "www.fit.hcmus.edu.vn"));
        dataArrayList.add(new MyData("Nguyen Quang Thuc", "www.fit.hcmus.edu.vn"));
        // step 3: declare an adapter
        MyDataAdapter adapter = new MyDataAdapter(this, dataArrayList);
        // step 4: set the adapter
        listView.setAdapter(adapter);
    }

    private void insertItemsAtRuntime() {
        // step 1: get the list view
        ListView listView = (ListView)findViewById(R.id.listViewMain);
// step 2: create an array list
        String[]    strings = {"Monday", "Tuesday", "Wednesday"};
        // step 3: declare an adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                strings);
// step 4: set the adapter
        listView.setAdapter(adapter);
    }
}
