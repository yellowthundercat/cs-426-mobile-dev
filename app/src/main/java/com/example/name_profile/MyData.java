package com.example.name_profile;

import java.io.Serializable;

class MyData implements Serializable {
    String name;
    String website;

    public MyData(String name, String website) {
        this.name = name;
        this.website = website;
    }
}
