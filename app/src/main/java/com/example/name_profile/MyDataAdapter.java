package com.example.name_profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

class MyDataAdapter extends ArrayAdapter<MyData> {
    private final Context context;

    public MyDataAdapter( Context context, ArrayList<MyData> arrayList) {
        super(context, 0, arrayList);
        this.context=context;
    }

    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        if (convertView==null) // not created yet
        {
            // how to create a row from a layout
            View v;
            if (position%2 == 0)
                v = createARowFromLayout(R.layout.layout_myitem);
            else v = createARowFromLayout(R.layout.layout_yellowitem);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView textViewName = v.findViewById(R.id.textViewName);
                    textViewName.setBackgroundColor(Color.GRAY);
                }
            });
            MyData data = getItem(position);
            bindDataToRow(v, data, position);
            convertView = v;
        }
        return convertView;
    }

    private void bindDataToRow(View rowView, MyData itemData, int position) {
        TextView textViewName = rowView.findViewById(R.id.textViewName);
        textViewName.setText(itemData.name);

        Button buttonWebsite = rowView.findViewById(R.id.buttonWebsite);
        buttonWebsite.setText(itemData.website);
        if (position%2 == 0)
            buttonWebsite.setBackgroundColor(Color.BLUE);

        buttonWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String website = ((Button)v).getText().toString();
                Uri webpage = Uri.parse("https://"+website);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                context.startActivity(intent);
            }
        });
    }

    private View createARowFromLayout(int layoutID) {
        return LayoutInflater.from(this.context).inflate(layoutID, null);
    }
}
